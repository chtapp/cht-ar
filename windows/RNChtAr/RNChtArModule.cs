using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Cht.Ar.RNChtAr
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNChtArModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNChtArModule"/>.
        /// </summary>
        internal RNChtArModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNChtAr";
            }
        }
    }
}
