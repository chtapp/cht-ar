
# react-native-cht-ar

## Getting started

`$ npm install cht-ar --save`

### Mostly automatic installation

`$ react-native link react-native-cht-ar`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-cht-ar` and add `RNChtAr.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNChtAr.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android


1. add flat dit to build.gradle 
   ````
   flatDir {
               dirs "$rootDir/../node_modules/cht-ar/libs"
           }
   ````
2. add library 
    ````
    packagingOptions {
           pickFirst 'lib/arm64-v8a/libc++_shared.so'
           pickFirst 'lib/armeabi-v7a/libc++_shared.so'
           pickFirst 'lib/x86/libc++_shared.so'
        }
    ````
 3. 在Android下加入
 ````
android {
    ndkVersion "21.3.6528147"
}
````

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNChtAr.sln` in `node_modules/react-native-cht-ar/windows/RNChtAr.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Cht.Ar.RNChtAr;` to the usings at the top of the file
  - Add `new RNChtArPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNChtAr from 'react-native-cht-ar';

// TODO: What to do with the module?
RNChtAr;
```
