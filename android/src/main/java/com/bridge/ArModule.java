package com.bridge;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.os.Handler;

import com.cht.ar.viewer.ChtarViewer;
import com.cht.ar.viewer.Language;
import com.cht.ar.viewer.ProjectType;
import com.cht.ar.viewer.ViewerException;
import com.cht.ar.viewer.callback.ImpResultCallback;
import com.facebook.react.bridge.ReadableMap;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;


public class ArModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    private static final String APP_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJjaHRhci5oaW5ldC5uZXQiLCJhdWQiOiJjaHRhciIsInN1YiI6IkNodEFyQXBwUmVhZGVyIiwiYWNjb3VudCI6ImNodGFyYXBwcmVhZGVyMSIsIm5hbWUiOiLkuIDoiKzkvb_nlKjogIUiLCJjdXN0b21lcklkIjoiY2h0YXIiLCJmcm9tTWVtYmVyU3lzIjoiY2h0YXJ1c2VycyIsImlhdCI6MTUzNjA0NjIwMCwiZXhwIjoxODUxNjYzNjAwfQ.olWa3TUdXa7zGRsvI09BvbBiJ1gogRXfAc0qNGZKCTu0dW8Oj0P-FcGjayVhXEt-J7mLv8dMIUXS6C8FoiL2Cou3nAdirg3OyrPsx-Qg6ptIBEHRrfAmCgytPrFNnU61hYtHC8fQaz6Q3AypDL0PzDQXY6_qPrLXIhwdik9Rpz0R1R3DTPXh8F6OSlNEvIL79K-C_cbRgv3ZGAbFvwCdVTHY_RdXF9bxkGBxTOin5n-peJ5MOHGiKhAFuyRCDDlfQzUXNieNPZlZyYolRJ2SSXZAxniI_YdSSrzDngr8LKxbXykZXYy_Pw8f2PufqOpcXxlZl3J65ZFyVu3bSHIAMQ";
    private static final String ACCOUNT_KEY = "WX/UQek3EuP8lwnZdI89xiO6g5PKDHvwrsOsevLoBZhAH9AagjojpgZeNbd6mdHig9IClMpIViOGlSdN1Mo+O9LM3YQWh9Rx2mr5VxQHosckwCPXmLYE1U28OA+/48jEGKfBkel3uDWbyA7UhZTSwNkxtqYRTfa0X0YfYoGCFd3OJ813kOei7kL/7arhUvzBVzJr+eaQ8k1y1lGceMQ9Xp8xwyl7HpvnO5UK5bVOSYwgkzwkyBrcKE6FIX8Acxyth/AeY4JxozvmFP8pr5aw+hnaV7M4McjEiBRPc6y3+NIebKmgN247xtq6aV0bzfz+aHaKXqdnzvf1hXYF3Wm3/Q==";
    private ChtarViewer mARViewer;

    public ArModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }


    @Override
    public String getName() {
        return "ArModule";
    }

    @ReactMethod
    public void show(String projectId, String projectTypeString) {
            ProjectType projectType = ProjectType.All;
            if (projectTypeString.equals("IMAGE")) {
                projectType = ProjectType.Image;
            }

            ActivityCompat.requestPermissions(reactContext.getCurrentActivity(),
                new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.RECORD_AUDIO},
                1);
                mARViewer = ChtarViewer.getInstance();
                // set activity context
                mARViewer.setContext(reactContext.getCurrentActivity());
                // set user interface language
                mARViewer.setLanguage(Language.En);
                // 設定連結測試區 (Optional)
                mARViewer.setEnvMode(ChtarViewer.EnvMode.Test);
                mARViewer.setAuthInfo("tangram.tw", ACCOUNT_KEY);
                // 檢查使用者身份是否有權限存取 AR 專案
        ProjectType finalProjectType = projectType;
        mARViewer.isProjectAuthorized(projectId, new ImpResultCallback() {
                    @Override
                    public void onResult(boolean success, boolean result, ViewerException e) {
                        if (success) {
                            if (result) {
                                Activity activity =reactContext.getCurrentActivity();
                                activity.runOnUiThread(() -> mARViewer.launchProject(projectId, finalProjectType));
                            } else {
                                Log.d("TAG", "無權限");
                            }
                        } else {
                            Log.d("TAG", e.getLocalizedMessage());
                        }
                    }
                });
     }

}
